#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 16:34:28 2021

@author: christiangross
DONE    I need to make the outpath, exposure, input all additional parameters in the jaf file
        jaf file thus it can be started, the submission script needs to be adjusted too.
DONE    Add an exclusion criterium which limits the GTEx tissues that are used.
        - Now I require a file to be added, containing all requested GTEx tissues.
DONE    Implement a safe_ensembl_coordinates which tests 10 times before cancelling the job. 
DONE    Implement logfile because the qsub logfiles are useless.
DONE    Implement time storage in log file
DONE    In case of GTEx exposure, select only target ensemblid variants, otherwise MAF filtering
        may take long time and the files are larger than necessary.
NOTE    Previous drop files may have Variants in them which would be removed anyhow because
        they are associated with genes that are not with the target gene, therefore these files
        are larger than necessary.
DONE    Implementing kidney_eQTL as co-regulatory check data.
TODO    Adding a parameter which specifies Interval data as the co-regulatory check data 
"""

import os
import sys
import argparse
import warnings
import pandas as pd
import numpy as np

# Modules
from merit import merit_errors
from merit_helper.mr_analysis import (maf_filter,save_empty)
from merit_helper.load_outcomes import (read_outcome,ensembl_coordinates)
import requests
import time
import pysam
import operator
import math

warnings.simplefilter('ignore')

########################################################################
#Constants
########################################################################
GTEX_PATH = '/lustre/projects/DTAdb/gwas_data/data'
gtex_key_dict = {'chr_name':0,'start_pos':1,'end_pos':2,
                'effect_allele':3,'other_allele':4,'strand':5,'uni_id':6,'var_id':7,
                'effect_size':8,'standard_error':9,'pvalue':10,'effect_allele_freq':11,
                'number_of_samples':12,'imputation_info':13,'het_i_square':14,'het_pvalue':15,
                'het_chi_square':16,'het_df':17,'source_row_idx':18,'analysis_type':19,
                'analysis_id':20,'effect_flipped':21,'ensembl_vep':22, 'sift':23,'polyphen':24,
                'clin_var':25,'ld_clump_080':26,'ld_clump_050':27,'ld_clump_001':28,
                'sojo_indep':29, 'tissue':30,'ensembl_gene_id':31,'uniprot_id':32}

INTERVAL_PATH = ''
interval_key_dict = {}

#path to manchester eQTL kidney data
KIDNEY_PATH = '/lustre/projects/DTAdb/downloads/manchester_eQTL_kidney/'
kidney_key_dict = {'uni_id':0, 'chr_name':1, 'start_pos':2, 'end_pos':3, 'effect_allele':4, 'other_allele':5, 
                   'assembly':6, 'ensembl_gene_id':7, 'effect_size':8, 'standard_error':9, 'pvalue':10}

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
def main():
    """
    The main entry point for the script -- just a place holder currently
    """
    ###  getting arguments
    parser = set_args()
    args = parse_args(parser)

    #retrieve gene list
    if os.path.isfile(args.ensemblids):
        ensemblids = retrieve_genes_from_file(args.ensemblids)
    else:
        ensemblids = args.ensemblids.strip().split(';')

    ### manipulating args
    paramlist, outcomes, exp_series = _manipulate_args(args)

    #checks if output directory exists, otherwise creates all missing directories
    check_create_output_dir(os.path.join(args.output))

    #create path to logfile.
    log_file = os.path.join(args.output,args.exposure+'_'+str(args.regulatory_pvalue)+'_logfile.txt')
    
    #checks if logfile exists, depending on that it creates one with a header or appends to an existing one.
    if os.path.isfile(log_file):
        logfile_handle = open(log_file,'a')
    else:
        logfile_handle = open(log_file,'w')
        logfile_handle.write('\t'.join(['EnsemblID','Minutes:Seconds','outfile_exists\n']))

    for ensemblid in ensemblids:
        start = time.time()
        ### Genetic coordinates
        coords = safe_ensembl_coordinates(ensemblid)
        paramlist['coordinates'] = (coords[0],coords[1]-args.up,coords[2]+args.down)

        if paramlist['coordinates'][1] < 0:
            paramlist['coordinates'] = (paramlist['coordinates'][0],1,paramlist['coordinates'][2])
            
        ### Get exposure
        #!!! I have to repeatedly redo the exp_series = outcomes.loc[exp] otherwith I get
        #AttributeError: 'Series' object has no attribute 'chrpos_spec'
        #this is an issue which needs to be solved in merit_helper.read_outcomes()
        exp_series = outcomes.loc[paramlist['exp']]

        try:
            exposure = get_exposure(exp_series, paramlist, args)
            #for gtex specific, check if column nsembl_gene_id exists and then filter for ensemblid
            if 'ensembl_gene_id' in exposure.columns:
                exposure = exposure[exposure.ensembl_gene_id == ensemblid]
            if 'gene_id' in exposure.columns: #for manchester kidney data
                exposure = exposure[exposure.gene_id == ensemblid]            
        except IsADirectoryError:
            logfile_handle.write('\t'.join([ensemblid,
                                           str(int((time.time()-start)/60))+
                                           ':'+str(int((time.time() - start)%60)),
                                           'False\n'])
                                 )
            logfile_handle.flush()
            continue

        try:
            ### Filter exposure
            # filter on p-value and maf
            exposure = filter_exposures(exposure, paramlist, args,ensemblid)

        except IsADirectoryError:
            logfile_handle.write('\t'.join([ensemblid,
                                           str(int((time.time()-start)/60))+
                                           ':'+str(int((time.time() - start)%60)),
                                           'False\n'])
                                 )
            logfile_handle.flush()
            continue

        #Filtering for co-regulatory active variants, using GTEx data
        exposure_drop,exposure_keep = filter_noco(exposure,ensemblid,
                                                  args.tissues,
                                                  plimit=args.regulatory_pvalue)

        exposure_drop.to_csv(os.path.join(args.output,ensemblid+"_dropfile.txt"),sep='\t')
        exposure_keep.to_csv(os.path.join(args.output,ensemblid+"_keepfile.txt"),sep='\t')
        logfile_handle.write('\t'.join([ensemblid,
                                       str(int((time.time()-start)/60))+':'+str(int((time.time() - start)%60)),
                                       'True\n'])
                             )
        logfile_handle.flush()

    logfile_handle.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def set_args(description=\
        'Set arguments to retrieve drop files.',
        **kwargs):
    """
    Setup command line arguments but do not parse them

    Parameters
    ----------
    description :   str, optional
    **kwargs    :   optional

    Returns
    -------
    parser : `argparse.ArgumentParser`
        A parser with all the arguments set up within it
    """
    parser = argparse.ArgumentParser(description=description, **kwargs)
    parser.add_argument('--input',
                    type=str,
                    help='path to a tab-delim text file listing the outcomes \
                            that need to be included in the analyses. Each \
                            outcome should have a separate row. No default -- \
                            see example directory.',
                    nargs='?',
                    default = '/home/rmgpcgr/create_drop_files/all_b37_outcomes.txt'
                    )
    parser.add_argument('--output',
                    type=str,
                    help='The name of the output directory. No defaults.',
                    nargs='?',
                    default='/home/rmgpcgr/Scratch/drop_files_kidney-eQTL_1e8/'
                    )
    parser.add_argument('--exposure',
                    type=str,
                    help='The row index of the `input`. The specified row will be \
                            used as exposure GWAS. \
                            No defaults.',
                    nargs='?',
                    default='ADM'
                    )
    parser.add_argument('--up',
                    type=int,
                    help='Upstream flank in bp (int),  \
                            default: 1000000.',
                    nargs='?',
                    default=1000000)
    parser.add_argument('--down',
                    type=int,
                    help='Downstream flank bp (int), \
                            default: 1000000.',
                    nargs='?',
                    default=1000000)
    parser.add_argument('--pvalue_selection',
                    type=int,
                    help='The -log10 p-value cut-off of the exposure (int).\
                    default: 4',
                    nargs='?',
                    default=4)
    parser.add_argument('--regulatory_pvalue',
                    type=int,
                    help='The -log10 p-value cut-off to decide at which point a co-regulatory\
                        interaction exists (int).\
                    default: 8',
                    nargs='?',
                    default=8)
    parser.add_argument('--maf',
                    type=float,
                    help='The MAF exposure cut-off (float). Default = 0.01.',
                    nargs='?',
                    default=0.01)
    parser.add_argument('--ensemblids',
                    type=str,
                    help='path to file containing the Ensembl IDs or a semicolon seperated list\
                        of human ensembl gene identifiers.',
                    nargs='?',
                    default='/lustre/home/rmgpcgr/cg_script_repo/eQTL_drop_files/target_gene_list.txt'
                    )
    parser.add_argument('--tissues',
                    type=str,
                    help='path to file containing a single column with all the tissues\
                    that are supposed to be used to check for co-regulaotry effect of variants.',
                    nargs='?',
                    default='/lustre/home/rmgpcgr/create_drop_files/manchester_kidney-eQTL_file_ls.txt'
                    )
    parser.add_argument('--logpval',
                    type=str,
                    help='True/False (default) indicator for -log10 exposure \
                            p-values.',
                    nargs='?',
                    default='True')
    parser.add_argument('--maf_path',
                    type=str,
                    help='TEMPORARY ARGUMENT!!.\
                    The path to the directory containing the per chromosome \
                    snpstat pre-calculated maf files. \
                    This will be depreciated soon, and will be replaced by \
                    improved MERIT config  \
                    files, just providing this a temporary interface. \
                    Defaults are set for myriad.',
                    nargs='?',
                    default= '/lustre/projects/ICS_UKB/genotypes/imp/qced2_eur/')
    return parser

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        A parser with all the arguments set up within it

    Returns
    -------
    args : :obj:`Namespace`
        An object with all the parsed command line arguments within it
    """
    args = parser.parse_args()
    return args

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _sets_test_parameters(args):
    """
    Manipualtes the Argparse.args parameter to contain test parameters.

    Parameters
    ----------
    args : :obj:`Namespace`
        Argparse parameters.

    Returns
    -------
    args : :obj:`Namespace`
        Argparse parameters after some values have been changed.

    """

    args.ensemblids = '/home/rmgpcgr/cg_script_repo/drop_files/pos-control_drug-targets.txt'
    args.up = 1000000
    args.down = 1000000
    args.exposure = "ADM"
    args.output = 'test_drop.txt'
    args.input = '/Users/christiangross/Documents/scripts/cg_script_repo/drop_files/\
        all_b37_outcomes.txt'
    return args
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _manipulate_args(args):
    """
    Manipulating the parsed arguments

    Returns
    -------
    paramlist: a list with model parameters.
    outcomes: a pandas dataframe files with the outcome file to use.
    exp_series:  a pandas dataframe with the parameters to read in the
            exposure file(s).
    """

    # parameter list to be returned
    paramlist = {}
    # Setting model specific settings
    # Pvalue cut-offs
    paramlist['pcut'] = args.pvalue_selection
    if args.logpval == 'False':
        paramlist['pcut'] = np.float_power(10, -paramlist['pcut'])
        
    # Logpval boolean
    paramlist['logpval'] = args.logpval == 'True'
    # maf filtering
    paramlist['mafcut'] = args.maf

    # Output path
    paramlist['MR_RES_PATH'] = args.output
    ### Loading outcome text file
    outcomes  = pd.read_csv(args.input, sep = '\t', index_col=0,engine='python')
    ### Setting exposure file
    exp = args.exposure
    # if not separate path use outcomes
    exp_series = outcomes.loc[exp]

    paramlist['exp'] = exp
    # return stuff
    return paramlist, outcomes, exp_series

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def safe_ensembl_coordinates(ensemblid, num_retries = 10):
    """
    This function calls safely merit_helper.ensembl_coordinates() multiple times and catches
    any errors raised by the request package.

    Parameters
    ----------
    ensemblid : str
        The human gene ensembl id for which gene regions should be returned.
    num_retries : int, optional
        The number of retries before a request error is thrown. The default is 10.

    Raises
    ------
    e
        Any error thrown by the request package.

    Returns
    -------
    tuple (str,int,int)
        The chromosome name, start postion in bp and end position in bp in 5' to 3' direction.
    """
    for attempt_no in range(num_retries):
        try:
            return ensembl_coordinates(ensemblid,upstream = 0, downstream=0)
        except requests.exceptions.RequestException as e:
            if attempt_no >= (num_retries - 1):
                raise e
            #in case that the attempt should be repeated, wait a second
            time.sleep(1)
            
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_exposure(exp_series, paramlist, ID):
    """
    Loading genetic associations with an exposure variable

    Parameters
    ----------
    exp_series : pandas.Series
        pandas Series which contains all the parameters necessary to read in the exposure GWAS.
    paramlist : list
        Containing the parsed input parameters.
    ID : TYPE
        DESCRIPTION.

    Returns
    -------
    exposure : pandas.DataFrame
        Dataframe containing the read in exposure variants.

    """
    # cis region
    try:
        exposure = read_outcome(exp_series,filter_regions = [paramlist['coordinates']])
    except (merit_errors.NoDataError, ZeroDivisionError) as e:
        _write_error(err=e, mess = 'No exposure data',
                identity=ID,
                filepath=paramlist['MR_RES_PATH'])
    ### returning stuff
    return exposure

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _write_error(err, mess, filepath, identity):
    """
    Local function that writes an empty file when an error is encountered.

    Arguments:
    ----------
    err:    str: the error
    mess:   str: an custom message to clarify or flag the error
    """
    save_empty(path=filepath,
            ensemblid=identity, error=mess)
    print(str(mess) + ": {0}'".format(identity),
            file=sys.stderr)
    print(err)
    sys.exit(0)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_create_output_dir(dirpath):
    """
    This function checks if all parts of the dirpath exists, otherwise creates them

    Parameters
    ----------
    dirpath : str
        Path to the directory in which the output files should be stored.

    Returns
    -------
    None.

    """
    tmp_path = '/'
    for path_elem in dirpath.strip().split('/'):
        if path_elem != '':
            tmp_path += path_elem+'/'
            if not os.path.isdir(tmp_path):
                os.mkdir(tmp_path)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def filter_exposures(exposure, paramlist, args,ensemblid, verbose = True):
    """
    Filter the exposure file on p-values and maf
    """
    ### Some minimal test
    if not (paramlist['mafcut'] > 0 and paramlist['mafcut'] <= 1):
        raise ValueError('Please specify a mafcut between 0 and 1')

    ### P-value cut off
    if verbose:
        print('P-value filtering for ' + str(exposure.shape[0]) + ' variants')

    #
    if paramlist['logpval']:
        exposure = exposure.loc[exposure.pvalue > paramlist['pcut']]
    else: #In case of logpval==False, the pvalue has already been un-logged
        exposure = exposure.loc[exposure.pvalue < paramlist['pcut']]
    # if no data
    if exposure.shape[0] == 0:
        _write_error(err='',
                     mess = 'No exposure data',
                     identity=ensemblid,
                     filepath=paramlist['MR_RES_PATH'])
    ### Maf filtering (using pre-calculated UKB snpstat files)
    if verbose:
        print('MAF filtering for ' + str(exposure.shape[0]) + ' variants')
    exposure = maf_filter(exposure, maf_cut=paramlist['mafcut'],
            maf_path=args.maf_path)
    # if no data
    if exposure.shape[0] == 0:
        _write_error(err='', mess = 'No exposure data',
                    identity=ensemblid,
                    filepath=paramlist['MR_RES_PATH'])
    ### return stuff
    return exposure

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def filter_noco(exposure,ensemblid,tissue_files,plimit=4,build_37=True):
    """
    This function iterates over the GTEx tissues that are specified in the parameters and checks
    if per Tissue, the selected SNPsn are unique.

    Parameters
    ----------
    exposure : pandas.DataFrame
        Containing all variants that survived the maf and pvalue filtering.
    ensemblid : str
        EnsemblID for which cis variants are selected, Is needed to identify if the variants are\
        associated with the target Ensembl ID in GTEx.
    plimit : int, optional
        Pvalue which defines a co-regulatory active variants. The default is 4.
    build_37 : bool, optional
        Indicates if GRCh37 or GRCh38 is used. The default is True which means GRCH37.

    Returns
    -------
    pandas.DataFrame
        Dataframe containing all potentially co-regulaotry active variants.
    pandas.DataFrame
        Dataframe containing all variants with a unique association with the target gene.

    """
    if build_37:
        build = 'b37'
    else:
        build = 'b38'

    #instead of finding and reading in all available tissue files, we read in a list of selected
    #tissue files
    with open(tissue_files,'r') as fh:
        tissue_file_list = [x.strip() for x in fh]

    tissue_snp_dict = {}
    #iterate over all tissue data files and retrieve exposure variants.
    for tissue in tissue_file_list:
        snp_bool_list = []

        #check what study the requested tissue belongs to, if GTEx, kidney-eQTL or INTERVAL
        if os.path.isfile(os.path.join(GTEX_PATH,tissue,'{}'.format(build),
                                       'data_files',
                                       tissue+'.{}'.format(build)+'.gnorm.gz'
                                       )
                          ):
            filepath = os.path.join(GTEX_PATH,tissue,'{}'.format(build),
                                    'data_files',
                                    tissue+'.{}'.format(build)+'.gnorm.gz')
            
            tissue_variables_dict = {'key_dict':gtex_key_dict,
                                     'operator':operator.gt,
                                     'pval_idx':gtex_key_dict['pvalue']}
            
            #gtex data is -10log transformed, therefore the check and transormation.
            #-10log.
            if plimit < 1:
                plimit = -math.log10(plimit)
            
            #reg_tissue_id = 'gtex'
        elif os.path.isfile(os.path.join(KIDNEY_PATH,tissue)):
            filepath = os.path.join(KIDNEY_PATH,tissue)
            
            tissue_variables_dict = {'key_dict':kidney_key_dict,
                                     'operator':operator.lt,
                                     'pval_idx':kidney_key_dict['pvalue']}
            
            #manchester_eQTL data is not yet logged normalized, therefore the pilimit needs to be
            #unlogged.
            if plimit > 1:
                plimit = 10**(-plimit)
            
            #reg_tissue_id = 'manchester_kidney'
        elif os.path.isfile(os.path.join(INTERVAL_PATH,tissue)):
            pass
            #reg_tissue_id = 'interval'
        else:
            sys.exit('no known path to any data file for co-regulatory variant check could be\
                     identified.')

        #load tabix indexed file
        tbx = pysam.TabixFile(filepath)
        #iterate over chromosome, position tuples
        for chrom, pos in zip(exposure.chr_name.values,exposure.start_pos.values):
            snp_batch_df = pd.DataFrame(_process_tabix(chrom,
                                                       pos-1,
                                                       pos,
                                                       plimit,
                                                       tissue_variables_dict,
                                                       tbx,0,0,
                                                       'start_pos','uni_id','pvalue','ensembl_gene_id'),
                                        columns=['pos','uni_id','pvalue','ensembl_gene_id'])

            #check if the snp in question is uniquly effective for the target gene only
            #if snp is missing in the dataset they are assumed to have a unique effect.
            if len(snp_batch_df.ensembl_gene_id.unique())==1 and snp_batch_df.ensembl_gene_id[0] == ensemblid:
                snp_bool_list.append(True)
            elif snp_batch_df.shape[0]==0:
                snp_bool_list.append(True)
            else:
                snp_bool_list.append(False)

        tissue_snp_dict[tissue] = snp_bool_list
    
    #a positive boolean value indicates if the snp-tissue combination is associated with
    #the target gene only or if no SNPs are known. False indicates more than 1 gene or the wrong
    #gene, in both cases the snp should be removed.
    tissue_snp_ar = np.array([tissue_snp_dict[x] for x in tissue_snp_dict])

    #this returns the exposure SNPs, which have more than 1 gene association in any tissue. (first array)
    #all snps that have a unique association (second array)
    return exposure[~np.all(tissue_snp_ar,0)],exposure[np.all(tissue_snp_ar,0)]

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _process_tabix(chrom,start,end,limit,tissue_variables_dict,tabix,dstreamlimit=1000000,ustreamlimit=1000000,*args):
    """
    Generator to process GTEx rows from the tabix.fetch() function. It yields all selected columns

    Parameters
    ----------
    chrom : str
        chromosome or sequence identifier of the target region.
    start : int
        start position of the target gene.
    end : int
        end position of the target gene.
    limit : float
        pvalue threshold as -log10.
    tissue_variables_dict : dict
        dictionary containing specifics depending on the regulatory tissue with which the
        individual variants are compared to.
    tabix : pysam.TabixFile
        Tabix file for which an iterator can be created.
    *args : str
        The column identifiers to select gtex columns. Valid column identifiers are
        chr_name, start_pos, end_pos, effect_allele, other_allele, strand, uni_id, var_id
        effect_size, standard_error, pvalue, effect_allele_freq, number_of_samples
        imputation_info, het_i_square, het_pvalue, het_chi_square, het_df, source_row_idx
        analysis_type, analysis_id, effect_flipped, ensembl_vep, sift, polyphen, clin_var
        ld_clump_080, ld_clump_050, ld_clump_001, sojo_indep	, tissue	, ensembl_gene_id
        uniprot_id

    Yield
    -------
    None : list of str
        each element is an element from a GTEx row.
    """
    for row in tabix.fetch(chrom,start-dstreamlimit,end+ustreamlimit):
        elements = row.split('\t')

        if tissue_variables_dict['operator'](
                float(elements[tissue_variables_dict['pval_idx']]),
                limit):
            yield [elements[tissue_variables_dict['key_dict'][x]] for x in args]

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def retrieve_genes_from_file(gene_file):
    """
    This function searches within the tab delimited gene file all human gene Ensembl Identifiers.

    Parameters
    ----------
    gene_file : str
        Path to file containing the human gene Ensembl identifiers. In case of multiple columns,
        they need to be tab delimited and the gene identifiers need to occupy a column on their own

    Returns
    -------
    ids : list of str
        List object containing all human gene Ensembl Identifiers that were found in the file.

    """
    #retrieve all ensembl human gene identifiers
    ids = []
    with open(gene_file) as gf:
        for lines in gf:
            line = lines.strip().split('\t')
            #check each element in the gene file for being an ensembl human gene identifier.
            for element in line:
                if element.startswith('ENSG0'):
                    ids.append(element)

        if len(ids) != 0:
            return ids
        sys.exit('No Ensembl Gene Identifiers were able to be retrieved from the provided data\
                 file ({})'.format(gene_file))

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
