#!/bin/bash
${HOME}/anaconda3/bin/python /home/rmgpcgr/create_drop_files/create_drop_file.py --output ${1} --exposure ${2} --regulatory_pvalue ${3} --ensemblids ${4} --input ${5} --logpval True